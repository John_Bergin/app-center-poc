﻿using App_Sample.ViewModels;
using System.ComponentModel;
using Xamarin.Forms;

namespace App_Sample.Views
{
    public partial class ItemDetailPage : ContentPage
    {
        public ItemDetailPage()
        {
            InitializeComponent();
            BindingContext = new ItemDetailViewModel();
        }
    }
}