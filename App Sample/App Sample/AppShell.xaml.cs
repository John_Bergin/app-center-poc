﻿using App_Sample.ViewModels;
using App_Sample.Views;
using System;
using System.Collections.Generic;
using Xamarin.Forms;

namespace App_Sample
{
    public partial class AppShell : Xamarin.Forms.Shell
    {
        public AppShell()
        {
            InitializeComponent();
            Routing.RegisterRoute(nameof(ItemDetailPage), typeof(ItemDetailPage));
            Routing.RegisterRoute(nameof(NewItemPage), typeof(NewItemPage));
        }

    }
}
